<?php
    // fichier : session1.php
    // ! ! ! démarrage de la session, instruction a placer en tête de script ! ! !
session_start();
?>
<!DOCTYPE>
    <html lang="fr" dir="ltr">
        <head>
            <script src="../js/tarteaucitron.js"></script>
            <script>
                tarteaucitron.init({
                "privacyUrl": "", /* URL de la page de la politique de vie privée */
                "hashtag": "#tarteaucitron", /* Ouvrir le panneau contenant ce hashtag */
                "cookieName": "tarteaucitron", /* Nom du Cookie */
                "orientation": "middle", /* Position de la bannière (top - bottom) */
                "showAlertSmall": true, /* Voir la bannière réduite en bas à droite */
                "cookieslist": true, /* Voir la liste des cookies */
                "adblocker": false, /* Voir une alerte si un bloqueur de publicités est
                détecté */
                "AcceptAllCta": true, /* Voir le bouton accepter tout (quand highPrivacy est à
                true) */
                "highPrivacy": true, /* Désactiver le consentement automatique : OBLIGATOIRE
                DANS l'UE */
                "handleBrowserDNTRequest": false, /* Si la protection du suivi du navigateur
                est activée, tout interdire */
                "removeCredit": false, /* Retirer le lien vers tarteaucitron.js */
                "moreInfoLink": true, /* Afficher le lien "voir plus d'infos" */
                "useExternalCss": false, /* Si false, tarteaucitron.css sera chargé */
                //"cookieDomain": ".my-multisite-domaine.fr", /* Cookie multisite - cas où SOUS DOMAINE */
                "readmoreLink": "/cookiespolicy" /* Lien vers la page "Lire plus" A FAIRE OU
                PAS */
                });
                (tarteaucitron.job = tarteaucitron.job || []).push('youtube');
            </script>
        </head>
        <body>
            <div class="youtube_player" videoID="PJCvmeRILLk" width="560" height="315"
            theme="light" rel="0" controls="1" showinfo="1" autoplay="0"></div>
            <?php
            echo 'On affecte 10 à la variable x<BR>';
            $x = 10;
            echo 'On ajoute une entree dans le tableau $_SESSION de visibilité super
            globale';
            $_SESSION["y"] = 10;
            ?>
        </body>
    </html>